package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	public int computeSum(IntegersBag bag){
		int sum=0;
		if(bag!= null){
			Iterator<Integer> iter= bag.getIterator();
			while(iter.hasNext()){
				sum+=iter.next();
			}
		}
		return sum;
	}
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	public int getFactorial(IntegersBag bag){
		int factorial=0;
		int value;
		if(bag!= null){
			Iterator<Integer> iter= bag.getIterator();
			while(iter.hasNext()){
				value= iter.next();
				factorial=value*factorial;
			}
		}
		return factorial;
	}
	public int getMin(IntegersBag bag){
		int min = Integer.MAX_VALUE;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( min>value){
					min=value;
				}
			}
		}
		return min;
	}
}
